﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject cameraObj;
    void FixedUpdate()
    {
        if (UI.userInterface.game.activeSelf)
            cameraObj.transform.Translate(MapGenerate.MG.speed * Time.deltaTime, 0, 0);
    }
}
