﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audVisual : MonoBehaviour
{
    public float sensitivity = 100;
    public float loudness = 0;
    private AudioSource _audioSource;
    public float[] _sapmles = new float[512];
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = Microphone.Start(Microphone.devices[0], true, 10, 44100);
        _audioSource.loop = true; 
        _audioSource.mute = false;
        while (!(Microphone.GetPosition("") > 0))
        {
        } 
        _audioSource.Play();
    }

    void Update()
    {
        loudness = GetSpectrumAudioSource() * sensitivity;
    }
    float GetSpectrumAudioSource()
    {
        _audioSource.GetSpectrumData(_sapmles, 0, FFTWindow.Blackman);
        float a = 0;
        foreach (float s in _sapmles)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }
}
