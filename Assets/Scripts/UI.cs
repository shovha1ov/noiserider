﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public static UI userInterface;

    public GameObject game;
    public GameObject gameUI;
    public GameObject moto;
    public GameObject pruebaUI;
    public GameObject settingsUI;
    public GameObject gameOverUI;
    public GameObject menu;
    public GameObject loudness;
    [Space(20)]
    public Text testText;
    public Text scoreText;
    public Text bestScoreText;
    public Text playTextBtn;
    public Text pruebaTextBtn;
    public Text settingsTextBtn;
    public Text restartTextBtn;
    public Text mainMenuTextBtn;
    public Text yourScoreText;
    public Text scoreOverTextBtn;
    [Space(20)]
    public Text languageText;
    public Text soundText;
    public Text onText;
    public Text offText;
    [Space(20)]
    public GameObject flag; 
    public Sprite[] flags; 
    public int iFlags = 0;

    public float score;
    public float bestScoreUI;


    void Start()
    {
        userInterface = this;
        menu.SetActive(true);
        game.SetActive(false);
        gameUI.SetActive(false);
        pruebaUI.SetActive(false);
        settingsUI.SetActive(false);
        gameOverUI.SetActive(false);
        loudness.SetActive(false);
    }

    void FixedUpdate()
    {
        bestScoreUI = PlayerPrefs.GetFloat("BestScore");
        if (PlayerPrefs.GetInt("iFlags") == 0)
        {
            flag.GetComponent<Image>().sprite = flags[0];
            testText.text = "Test";                   
            playTextBtn.text = "Play";
            pruebaTextBtn.text = "Prueba";
            settingsTextBtn.text = "Settings";
            languageText.text = "Language";
            soundText.text = "Sound";
            restartTextBtn.text = "Restart";
            mainMenuTextBtn.text = "Main menu";
            yourScoreText.text = "Your score:";
            scoreText.text = "Score: " + score.ToString("F0");
            scoreOverTextBtn.text = "" + score.ToString("F0");
            bestScoreText.text = "Best score: " + bestScoreUI.ToString("F0");
        }
        else if (PlayerPrefs.GetInt("iFlags") == 1)
        {
            flag.GetComponent<Image>().sprite = flags[1];
            testText.text = "Тест";
            playTextBtn.text = "Игра";
            pruebaTextBtn.text = "Тест";
            settingsTextBtn.text = "Настройки";
            languageText.text = "Язык";
            soundText.text = "Звук";
            restartTextBtn.text = "Рестарт";
            mainMenuTextBtn.text = "Главное меню";
            yourScoreText.text = "Ваш результат:";
            scoreText.text = "Очки: " + score.ToString("F0");
            scoreOverTextBtn.text = "" + score.ToString("F0");
            bestScoreText.text = "Лучший результат: " + bestScoreUI.ToString("F0");
        }
        else
        {
            flag.GetComponent<Image>().sprite = flags[2];
            testText.text = "Prueba";
            playTextBtn.text = "Juega";
            pruebaTextBtn.text = "Prueba";
            settingsTextBtn.text = "Ajustes";
            languageText.text = "Idioma";
            soundText.text = "Sonido";
            restartTextBtn.text = "Reiniciar";
            mainMenuTextBtn.text = "Menú principal";
            yourScoreText.text = "Tu puntuación:";
            scoreText.text = "Puntaje: " + score.ToString("F0");
            scoreOverTextBtn.text = "" + score.ToString("F0");
            bestScoreText.text = "Mejor puntuación: " + bestScoreUI.ToString("F0");
        }
    }
}
