﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotoMove : MonoBehaviour
{
    public static MotoMove MM;
    public float motoSpeed;
    public float angleRotate;
    public WheelJoint2D[] wheelJoints;
    JointMotor2D frontWheel;
    JointMotor2D backWheel;

    public bool isLeft = false;
    public bool isRight = false;

    void Start()
    {
        MM = this;
        wheelJoints = gameObject.GetComponents<WheelJoint2D>();
        backWheel = wheelJoints[0].motor;
        frontWheel = wheelJoints[1].motor;
    }
    void FixedUpdate()
    {
        backWheel.motorSpeed = MapGenerate.MG.speed;
        frontWheel.motorSpeed = MapGenerate.MG.speed;
        if (isLeft || Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, angleRotate * Time.deltaTime);
        }
        if (isRight || Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -angleRotate * Time.deltaTime);
        }
    }
}
