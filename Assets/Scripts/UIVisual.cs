﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIVisual : MonoBehaviour
{
    public GameObject[] sprites;
   
    void Start()
    {
        sprites[0].SetActive(true);
    }
    void FixedUpdate()
    {

        if (transform.position.y - 2 >= -1.75)
        {
            sprites[1].SetActive(true);
        }
        else
        {
            sprites[1].SetActive(false);
        }
        if (transform.position.y - 2 >= -1.25)
        {
            sprites[2].SetActive(true);
        }
        else
        {
            sprites[2].SetActive(false);
        }
        if (transform.position.y - 2 >= -0.75)
        {
            sprites[3].SetActive(true);
        }
        else
        {
            sprites[3].SetActive(false);
        }
        if (transform.position.y - 2 >= -0.25)
        {
            sprites[4].SetActive(true);
        }
        else
        {
            sprites[4].SetActive(false);
        }
        if (transform.position.y - 2 >= 0.25)
        {
            sprites[5].SetActive(true);
        }
        else
        {
            sprites[5].SetActive(false);
        }
        if (transform.position.y - 2 >= 0.75)
        {
            sprites[6].SetActive(true);
        }
        else
        {
            sprites[6].SetActive(false);
        }
        if (transform.position.y - 2 >= 1.25)
        {
            sprites[7].SetActive(true);
        }
        else
        {
            sprites[7].SetActive(false);
        }
        if (transform.position.y - 2 >= 1.75)
        {
            sprites[8].SetActive(true);
        }
        else
        {
            sprites[8].SetActive(false);
        }
        if (transform.position.y - 2 >= 2.25)
        {
            sprites[9].SetActive(true);
        }
        else
        {
            sprites[9].SetActive(false);
        }
        if (transform.position.y - 2 >= 2.75)
        {
            sprites[10].SetActive(true);
        }
        else
        {
            sprites[10].SetActive(false);
        }
        if (transform.position.y - 2>= 3.25)
        {
            sprites[11].SetActive(true);
        }
        else
        {
            sprites[11].SetActive(false);
        }
    }
}
