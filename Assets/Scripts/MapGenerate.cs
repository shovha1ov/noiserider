﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerate : MonoBehaviour
{
    public static MapGenerate MG;
    public float speed;
    public float upSpeed;
    [Range(-1f, 3.25f)]
    public float yPos;

    public GameObject micVolume;

    public bool minPos;
    void Start()
    {
        MG = this;
    }
    void FixedUpdate()
    {
        yPos = micVolume.GetComponent<audVisual>().loudness * upSpeed;
        transform.Translate(speed * Time.deltaTime, 0, 0);
        if (transform.position.y <= 5.5)
        {
            if(yPos > 0.005f)
            {
                transform.Translate(0, yPos, 0);
                if (transform.position.y > 0)
                {
                    minPos = false;
                }
            }
        }
        if (yPos < 0.005f)
        {
            if (minPos == false)
                transform.Translate(0, -yPos * 30, 0);
            if (transform.position.y < 0)
            {
                minPos = true;
            }
        }    
    }
}
