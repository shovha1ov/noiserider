﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public static Score SC;
    public float timeStart;
    public float bestScore;
    public bool isPlay;

    void Start()
    {
        SC = this;
    }
    void FixedUpdate()
    {
        bestScore = PlayerPrefs.GetFloat("BestScore");
        if (isPlay)
        {
            timeStart += Time.deltaTime;
            UI.userInterface.score = timeStart;
        }
        if (UI.userInterface.score > bestScore)
            PlayerPrefs.SetFloat("BestScore", UI.userInterface.score);
    }
}

