﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public AudioSource myFx;
    public AudioClip crashFx;
    public GameObject road;

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Moto")
        {
            myFx.PlayOneShot(crashFx);
            myFx.volume = 0.6f;
            Score.SC.isPlay = false;
            Destroy(LinesDrawer.LD.currentLine.gameObject);
            UI.userInterface.game.SetActive(false);
            UI.userInterface.gameUI.SetActive(false);
            UI.userInterface.loudness.SetActive(false);
            UI.userInterface.gameOverUI.SetActive(true);       
            OnPlay.OP.moto.transform.position = OnPlay.OP.motoTemp;
            OnPlay.OP.road.transform.position = OnPlay.OP.roadTemp;
            OnPlay.OP.cameraUI.transform.position = OnPlay.OP.cameraTemp;
        }
    }
}
