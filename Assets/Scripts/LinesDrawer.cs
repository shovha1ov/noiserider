﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinesDrawer : MonoBehaviour
{
	public static LinesDrawer LD;
	public GameObject linePrefab;
	public GameObject startPoint;
	public LayerMask cantDrawOverLayer;
	int cantDrawOverLayerIndex;

	[Space(30f)]
	public Gradient lineColor;
	public float linePointsMinDistance;
	public float lineWidth;

	public LineMap currentLine;

	Camera cam;

	void Start()
	{
		LD = this;
		cam = Camera.main;
		cantDrawOverLayerIndex = LayerMask.NameToLayer("CantDrawOver");
	}

	void FixedUpdate()
	{

		if (currentLine == null)
			BeginDraw();
			

		if (currentLine != null)
			Draw();
	}
	void BeginDraw()
	{
		currentLine = Instantiate(linePrefab, this.transform).GetComponent<LineMap>();

		currentLine.SetLineColor(lineColor);
		currentLine.SetPointsMinDistance(linePointsMinDistance);
		currentLine.SetLineWidth(lineWidth);

	}
	void Draw()
	{
		Vector2 groundPosition = startPoint.transform.position;

		Physics2D.CircleCast(groundPosition, lineWidth / 3f, Vector2.zero, 1f, cantDrawOverLayer);

		currentLine.AddPoint(groundPosition);
	}
}
