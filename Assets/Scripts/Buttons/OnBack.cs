﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnBack : MonoBehaviour
{
    public void DownBack()
    {
        SceneManager.LoadScene(0);
    }
}
