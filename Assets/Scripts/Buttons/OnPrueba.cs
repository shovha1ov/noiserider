﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPrueba : MonoBehaviour
{
    public void DownPrueba()
    {
        UI.userInterface.menu.SetActive(false);
        UI.userInterface.game.SetActive(true);
        UI.userInterface.loudness.SetActive(true);
        UI.userInterface.moto.SetActive(false);
        UI.userInterface.pruebaUI.SetActive(true);
    }
}
