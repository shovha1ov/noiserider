﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSettings : MonoBehaviour
{
    public void DownSettings()
    {
        UI.userInterface.settingsUI.SetActive(true);
        UI.userInterface.menu.SetActive(false);
    }
}
