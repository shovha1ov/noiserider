﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnSound : MonoBehaviour
{
    public static OnSound OS;
    public GameObject mainCamera;
    public GameObject onBtn;
    public GameObject offBtn;
    public Color rgbColorDown;
    public Color rgbColorUP;
    public int isSound;
    void Start()
    {
        OS = this;
        isSound = PlayerPrefs.GetInt("OnSound");
    }
    void Update()
    {
        if (isSound == 1)
        {
            onBtn.GetComponent<Text>().color = rgbColorDown;
            offBtn.GetComponent<Text>().color = rgbColorUP;
            mainCamera.GetComponent<AudioListener>().enabled = true;
        }
        if (isSound == 0)
        {
            offBtn.GetComponent<Text>().color = rgbColorDown;
            onBtn.GetComponent<Text>().color = rgbColorUP;
            mainCamera.GetComponent<AudioListener>().enabled = false;
        }
    }
    public void isOn()
    {
        isSound = 1;
        PlayerPrefs.SetInt("OnSound", isSound);
    }
    public void isOff()
    {
        isSound = 0;
        PlayerPrefs.SetInt("OnSound", isSound);  
    }
}
