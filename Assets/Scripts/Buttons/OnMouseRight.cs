﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseRight : MonoBehaviour
{
    public void OnMouseUp()
    {
        MotoMove.MM.isRight = false;
    }
    public void OnMouseDown()
    {
        MotoMove.MM.isRight = true;
    }
}
