﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnRestart : MonoBehaviour
{
    public void DownRestart()
    {
        Score.SC.isPlay = true;
        Score.SC.timeStart = 0;
        UI.userInterface.game.SetActive(true);
        UI.userInterface.gameUI.SetActive(true);
        UI.userInterface.loudness.SetActive(true);
        UI.userInterface.gameOverUI.SetActive(false);
    }
}
