﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlay : MonoBehaviour
{
    public static OnPlay OP;
    public GameObject road;
    public Vector3 roadTemp;
    public GameObject moto;
    public Vector3 motoTemp;
    public GameObject cameraUI;
    public Vector3 cameraTemp;


    private void Start()
    {
        OP = this;
    }
    public void DownPlay()
    {
        roadTemp = road.transform.position;
        motoTemp = moto.transform.position;
        cameraTemp = cameraUI.transform.position;
        Score.SC.isPlay = true;       
        UI.userInterface.menu.SetActive(false);
        UI.userInterface.game.SetActive(true);
        UI.userInterface.gameUI.SetActive(true);
        UI.userInterface.loudness.SetActive(true);
    }
}
