﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseLeft : MonoBehaviour
{
    private void OnMouseUp()
    {
        MotoMove.MM.isLeft = false;
    }
    private void OnMouseDown()
    {
        MotoMove.MM.isLeft = true;
    }
}
